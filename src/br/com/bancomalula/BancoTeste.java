package br.com.bancomalula;

public class BancoTeste {

	public static void main(String[] args) {
		
		Conta.saldoDoBanco = 5_000.00;
		
		Conta conta = new Conta( 1, "Corrente", 2_750.00, "123!", new Cliente ("Caue", "408888888", "333333333", "caue@senai.com", Sexo.MASCULINO));
		
		conta.exibeSaldo();
		conta.deposita(50);
		conta.exibeSaldo();
		System.out.println("Cofre: " + conta.saldoDoBanco);
		conta.saca(100);
		conta.exibeSaldo();
		System.out.println("Cofre: " + conta.saldoDoBanco);
		
		// Mostrar os dados da conta
		System.out.println("DADOS DA CONTA: ");
		System.out.println("N� " + conta.getNumero());
		System.out.println("Tipo: " + conta.getTipo());
		System.out.println("Senha: " + conta.getSenha());
		System.out.println("Saldo: " + conta.getSaldo());
		System.out.println("Titular: " + conta.getCliente().getNome());
		System.out.println("CPF: " + conta.getCliente().getCpf());
		System.out.println("RG: " + conta.getCliente().getRg());
		System.out.println("E-mail: " + conta.getCliente().getEmail());
		System.out.println("Sexo: " + conta.getCliente().getSexo().nome);

	}

}
